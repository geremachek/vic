<#
VIC created by geremachek for the windows command line
Free to modify and distribute
#>
function Read-HostCustom {
	param($Prompt)
	Write-Host $Prompt -NoNewline
	$Host.UI.ReadLine()
}
$host.ui.RawUI.WindowTitle = "[VIC]"
$line_no = 0
$fsrc = @()
Clear-Host
Write-Host "[VIC]`n"
$fname = Read-HostCustom -Prompt "File Name: "
New-Item $fname
Clear-Host
$host.ui.RawUI.WindowTitle = "[$fname]"
Write-Host "[$fname]"
Write-Host " "
while(1 -eq 1){

	$line_no = $line_no+1
	$fui = Read-HostCustom -Prompt "$line_no  "
	if($fui -eq ":ex"){
		Set-Content $fname $fsrc
		Clear-Host
		Write-Host "Done Editing $fname"
		$host.ui.RawUI.WindowTitle = "PS"
		break
	}else{
		$fsrc += ,$fui
	}
}
